// The present software is not subject to the US Export Administration Regulations (no exportation license required), May 2012
package com.example.myapplication.database;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

public class DatabaseArrayAdapter extends ArrayAdapter<DatabaseItem>
{
	private Context				c;
	private int					id;
	private List<DatabaseItem>	items;

	public DatabaseArrayAdapter(Context context, int textViewResourceId, List<DatabaseItem> objects)
	{
		super(context, textViewResourceId, objects);
		c = context;
		id = textViewResourceId;
		items = objects;
	}

	public DatabaseItem getItem(int i)
	{
		return items.get(i);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		View v = convertView;
		if (v == null)
		{
			LayoutInflater vi = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			v = vi.inflate(id, null);
		}
		final DatabaseItem o = items.get(position);
		if (o != null)
		{


				try
				{
					int color = Color.TRANSPARENT;
					if (o.isSelected())
					{
						color = Color.CYAN;
					}
				}
				catch (Exception e)
				{
				}

		}
		return v;
	}
}
