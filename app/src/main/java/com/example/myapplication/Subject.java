package com.example.myapplication;

import com.machinezoo.sourceafis.FingerprintTemplate;

import java.io.Serializable;

public class Subject implements Serializable {
    private int id;
    private String name;
    private byte[] template;

    // Construtor
    public Subject(int id, String name, byte[] template) {
        this.id = id;
        this.name = name;
        this.template = template;
    }

    // Getters
    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public byte[] getTemplate() {
        return template;
    }
}